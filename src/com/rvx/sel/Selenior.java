package com.rvx.sel;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Function;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Selenior{
    public static void main(String[] args){
    	
    	String URL = args[0];
	
 	// File pathBinary = new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
 	// FirefoxBinary firefoxBinary = new FirefoxBinary(pathBinary);
	// DesiredCapabilities desired = DesiredCapabilities.firefox();
 	// FirefoxOptions options = new FirefoxOptions();
 	// desired.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));
	/*Falls geckodriver.exe sich in gleichem Pfad wie der Firofox befindet, dann sollte 
	die folgende Zeile kommentiert werden*/
    
    	System.setProperty("webdriver.gecko.driver", "C:\\Programs\\geckodriver.exe");
    	println("NEUE INFO:" + System.getProperty("webdriver.gecko.driver"));
		WebDriver driver = new FirefoxDriver();
    	//driver.manage().window().maximize();
    	WebElement body = driver.findElement(By.tagName("body"));
    	
    // body.sendKeys(Keys.F11); St�rtzt den normalen Eingang, also auskommentiert wurde
	// FirefoxDriver ff = new FirefoxDriver(new FirefoxBinary(new java.io.File()), new FirefoxProfile());
	// WebDriver driver = new FirefoxDriver();
	// driver.navigate().to("https://duckduckgo.com");
	//driver.get(URL);
    
    	try {
    	driver.get(URL);
    	} catch (Exception ex) {
    	println("DRIVER.GET PROBLEM!!!");
    	end();
    	System.exit(0);
    	}
    	
    	WebDriverWait wait = new WebDriverWait(driver, 15);  // you can reuse this one
    	//driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    	String html_class = "ui-autocomplete-input";
    	String xpath_search_box = "//input[@class='" + html_class + "']";
    	String xpath_search_btn = "//button[@type='" + "submit" + "']";
    	String search_text = args[1];
    	String xpath_megaphoto = "//a[@class='" + "megaPhotoLink megaPhoto" + "']";
    	String xpath_photo_sum = "//div[@class='classifiedDetailThumbListContainer']//span[@class='images-count'][1]"; 
    	
    	String xpath_right_nav_link = "//div[@class='megaPhotoImgContainer']/a[@class='megaPhotoRight']";
    	String xpath_megaphoto_img_url = "//div[@class='megaPhotoImgContainer']/div[@class='mega-photo-img']/img";
    	Integer sum_Mega_Photos = 2;
    	
    	ArrayList<String> links = new ArrayList<String>();
    	String link = "";
    	
    	println("XPATH EXPRESSION:  " + xpath_search_box);
    	println("MEGAPHOTO EXPRESSION: " + xpath_megaphoto);
    
    	try { 
    		WebElement elt = driver.findElement(By.xpath(xpath_search_box));
    		elt.click();
    		elt.sendKeys(search_text);
    		
    		WebElement search_btn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_search_btn)));
    		search_btn.click();
    		
    	} catch (Exception ex) {
    		println("WebElement SEARCH TEXT wurde nicht gefunden k�nnen");
    		}
    	
    	try {
    		WebElement megaphoto_link = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_megaphoto)));
    		megaphoto_link.click();
    		
    		WebElement sum = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_photo_sum)));
    		sum_Mega_Photos = findPhotoSum(sum.getText());
    		println("PHOTO SUM: " + String.valueOf(sum_Mega_Photos));
    		
    		megaphoto_link.click();
    	} catch (Exception ex) {
    		System.out.println("WebElement MEGA-PHOTO oder SUM wurde nicht gefunden k�nnen");
		}
    	
    	try {
       		WebElement right_nav = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_right_nav_link)));
       		link = "dummy";
       		links.add(link); // links index 0 wird mit dem dummy besetzt werden.
       		WebElement mega_image;
    		
       		for (Integer i=1; i<=sum_Mega_Photos; i++) {
    			
       			mega_image = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath_megaphoto_img_url)));
    			link = mega_image.getAttribute("src");
    				
    			if (links.get(i-1) != link) {
    				links.add(link);
    				// println(i.toString() + ". link added: " + link);
    			}
    			else {
    				link ="Link nicht gefunden";
    				links.add(link);
    			}
    			java.util.concurrent.TimeUnit.SECONDS.sleep(1);
    			right_nav.click();
       		}
       	} catch (Exception ex) {
    		println("WebElement xpath_right_nav_link wurde nicht gefunden k�nnen");
    		}
    	
    	println("Die Gr��e der Links: " + String.valueOf(links.size()-1));
    	for (String s : links ) {
    		println(s);
    	}
    	end();
    }
    public static void end()
    {
        	println("Programm wurde beendet...");
        	
        	try {
        		killProcess("geckodriver.exe");
        		println("GECKODRIVER wurde erfolgreich beendet...");
        	} catch(Exception ex) {
        		println("Ugh!..");
        		ex.printStackTrace();
    	    }
    }

    // task management
    private static final String TASKLIST = "tasklist";
    private static final String KILL = "taskkill /F /IM ";

    public static void killProcess(String serviceName) throws Exception {
    	Runtime.getRuntime().exec(KILL + serviceName);
    }
    
    public static boolean isProcessRunning(String serviceName) throws Exception {
    	Process p = Runtime.getRuntime().exec(TASKLIST);
    	BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
    	String line;
    	while ((line = reader.readLine()) != null) {
    		println(line);
    		if (line.contains(serviceName)) {
    			return true;
    		}
    	}
	return false;	
    }
    
    public static int findPhotoSum (String strExp) {
    	println("STR EXP: " + strExp);
	    Pattern pat = Pattern.compile("([0-9]+)"); //Regul�re Ausdr�cke um Nummers�tze zu finden
	    Matcher m = pat.matcher(strExp);
	    m.find();//First number set X out of X/XX 
	    m.find();//Zweite Nummersatz, XX von X/XX
	    String sum = m.group(1);
	    println(sum);
	    return Integer.parseInt(sum);
    }
    
    public WebElement fluentWait(final By locator, WebDriver driver) {
		Wait<WebDriver> warten = new FluentWait<WebDriver>(driver)
                .withTimeout(30, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(NoSuchElementException.class);

        WebElement foo = warten.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(locator);
            }
        });
        return  foo;
    };
    
    public static void println(Object line) {
	System.out.println(line);
    }
}