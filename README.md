# Selenior #

*Bir Websitesindeki resimleri tam boyutlu olarak çekip listeleyen Java uygulaması*
*Eine Java-Anwendung, die die Bilder in voller Größe aus einer Website herauszieht und auflistet.*

Uygulama şu videoda ( Selenior-Bildschirm-Aufnahme.mp4 ) çalışma anında görülebilir.
Die Anwendung kann in diesem Screencastvideo ( Selenior-Bildschirm-Aufnahme.mp4 ) beim Einsatz gesehen werden.

![Demo Video](Selenior-Bildschirm-Aufnahme.mp4)

Bu proje `Eclipse-4.7` *Oxygen* üzerinde inşa edilmiştir.
Dieses Projekt wurde auf der `Eclipse-4.7` *Oxygen* aufgebaut. 

Son Düzenleme 29.09.2020 18:48
Zuletzt bearbeitet 29.09.2020 18:48 (Früher 02.09.2019 03:16)
Zuletzt bearbeitet 02.09.2019 03:16
Oluşturuldu 17.10.2018 17:30
Erstellt 17.10.2018 17:30
